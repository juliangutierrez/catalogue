class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.text :description

      t.timestamps
    end

    create_table :countries_developing_projects, id: false do |t|
    	t.belongs_to :country, index: true
      t.belongs_to :project, index: true
    end

     create_table :countries_using_projects, id: false do |t|
    	t.belongs_to :country, index: true
      t.belongs_to :project, index: true
    end

     create_table :countries_interested_in_projects, id: false do |t|
    	t.belongs_to :country, index: true
      t.belongs_to :project, index: true
    end
  end
end
