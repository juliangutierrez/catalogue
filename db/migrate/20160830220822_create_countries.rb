class CreateCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :countries do |t|
      t.string :name
      t.string :slung
      t.binary :flag

      t.timestamps
    end
  end
end
