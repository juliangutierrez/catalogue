class Project < ApplicationRecord
  has_and_belongs_to_many :developing_countries, class_name: 'Country', :join_table => :countries_developing_projects
  has_and_belongs_to_many :interested_by_countries, class_name: 'Country', :join_table => :countries_interested_in_projects
  has_and_belongs_to_many :used_by_countries, class_name: 'Country', :join_table => :countries_using_projects
end
